Image for pushing docker images to gcr.io directly from GitLab CI docker-build stage.

Based on docker:latest with instructions for getting google-cloud-sdk & components installed.

An example of `stage` for GitLab CI 9.X `.gitlab-ci.yml`:


```
#!shell

docker-build-redis:
  stage: package
  image: bicollect/docker-gcloud:latest
  only:
  - /^redis-.*$/
  before_script:
  - echo "$GOOGLE_KEY" > key.json
  script:
  - export DOCKER_HOST="tcp://localhost:2375"
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN gitlab.example.com:5005
  - docker build -f ./Dockerfile-redis -t gcr.io/$GOOGLE_PROJECT_NAME/redis:$CI_JOB_TOKEN .
  - docker tag gcr.io/$GOOGLE_PROJECT_NAME/redis:$CI_JOB_TOKEN gcr.io/$GOOGLE_PROJECT_NAME/redis:latest
  - gcloud auth activate-service-account --key-file key.json
  - gcloud docker -- push gcr.io/$GOOGLE_PROJECT_NAME/redis:$CI_JOB_TOKEN
  - gcloud docker -- push gcr.io/$GOOGLE_PROJECT_NAME/redis:latest
```


`$GOOGLE_KEY` and `$GOOGLE_PROJECT_NAME` variables stands for project Secret Variables, configured on the GitLab CI/CD pipelines settings page.
